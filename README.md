# jupyter-perl

## Description

Perl5 with IPython running the IPerl kernel. Please install additional Perl packages using the **cpanm** script provided in this distribution.

For more info on the IPerl package please visit: https://metacpan.org/source/ZMUGHAL/Devel-IPerl-0.006

Added some default Perl modules to save some time in the long run:

* PDL
* Moose
* MooseX::AbstractFactory
* MooseX::AbstractMethod
* MooseX::Storage
* Test::More

And of course the additional dependencies that come with these packages...

## Deployment

	docker run --restart always --name jupyter-perl -d -p 8080:8080 -v /opt/jupyter:/opt/jupyter stuffox/jupyter-perl

### Environment variables

### Ports and IPs

By default the jupyter instance runs on port 8080 with a bind on 0.0.0.0 interface, you can change that by tinkering with the **JUPYTER_PORT** and **JUPYTER_IP** variables.

	JUPYTER_PORT 8080
	JUPYTER_IP 0.0.0.0

#### Notebook directory

Take control of you notebooks by setting your own path for the notebook directory. It defaults to the path listed below:

	JUPYTER_NOTEBOOK_DIR /opt/jupyter

## Contact

For details or suggestions you can find me at <tudor@marghidanu.com>
